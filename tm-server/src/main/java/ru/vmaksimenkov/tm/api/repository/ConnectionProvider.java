package ru.vmaksimenkov.tm.api.repository;

import org.jetbrains.annotations.NotNull;

import java.sql.Connection;

public interface ConnectionProvider {

    @NotNull Connection getConnection();

}

package ru.vmaksimenkov.tm.exception.entity;

public class NoTasksException extends RuntimeException {

    public NoTasksException() {
        super("Error! No tasks...");
    }

}

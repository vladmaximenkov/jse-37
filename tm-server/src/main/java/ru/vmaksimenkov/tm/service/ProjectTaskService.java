package ru.vmaksimenkov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.IProjectRepository;
import ru.vmaksimenkov.tm.api.repository.ITaskRepository;
import ru.vmaksimenkov.tm.api.service.IConnectionService;
import ru.vmaksimenkov.tm.api.service.IProjectTaskService;
import ru.vmaksimenkov.tm.exception.empty.EmptyIdException;
import ru.vmaksimenkov.tm.exception.empty.EmptyNameException;
import ru.vmaksimenkov.tm.exception.entity.ProjectNotFoundException;
import ru.vmaksimenkov.tm.exception.system.IndexIncorrectException;
import ru.vmaksimenkov.tm.model.Task;
import ru.vmaksimenkov.tm.repository.ProjectRepository;
import ru.vmaksimenkov.tm.repository.TaskRepository;
import ru.vmaksimenkov.tm.repository.UserRepository;

import java.sql.Connection;
import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.checkIndex;
import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public class ProjectTaskService implements IProjectTaskService {

    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private IConnectionService connectionService;

    @Override
    @SneakyThrows
    public void bindTaskByProjectId(@NotNull final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        if (isEmpty(projectId) || isEmpty(taskId)) throw new EmptyIdException();
        try (Connection connection = connectionService.getConnection()) {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final TaskRepository taskRepository = new TaskRepository(connection);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            taskRepository.bindTaskPyProjectId(userId, projectId, taskId);
        }
    }

    @Override
    @SneakyThrows
    public void clearTasks(@NotNull final String userId) {
        try (Connection connection = connectionService.getConnection()) {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final TaskRepository taskRepository = new TaskRepository(connection);
            taskRepository.removeAllBinded(userId);
            projectRepository.clear(userId);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAllTaskByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        try (Connection connection = connectionService.getConnection()) {
            @NotNull final TaskRepository taskRepository = new TaskRepository(connection);
            if (!taskRepository.existsByProjectId(userId, projectId)) return null;
            return taskRepository.findAllByProjectId(userId, projectId);
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectById(@NotNull final String userId, @Nullable final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        try (Connection connection = connectionService.getConnection()) {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final TaskRepository taskRepository = new TaskRepository(connection);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeById(userId, projectId);
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByIndex(@NotNull final String userId, @NotNull final Integer projectIndex) {
        try (Connection connection = connectionService.getConnection()) {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final TaskRepository taskRepository = new TaskRepository(connection);
            if (!checkIndex(projectIndex, projectRepository.size(userId))) throw new IndexIncorrectException();
            @Nullable final String projectId = projectRepository.getIdByIndex(userId, projectIndex);
            if (isEmpty(projectId)) throw new EmptyIdException();
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeOneByIndex(userId, projectIndex);
        }
    }

    @Override
    @SneakyThrows
    public void removeProjectByName(@NotNull final String userId, @Nullable final String projectName) {
        if (isEmpty(projectName)) throw new EmptyNameException();
        try (Connection connection = connectionService.getConnection()) {
            @NotNull final ProjectRepository projectRepository = new ProjectRepository(connection);
            @NotNull final TaskRepository taskRepository = new TaskRepository(connection);
            @Nullable final String projectId = projectRepository.getIdByName(userId, projectName);
            if (isEmpty(projectId)) throw new EmptyIdException();
            taskRepository.removeAllByProjectId(userId, projectId);
            projectRepository.removeOneByName(userId, projectName);
        }
    }

    @Override
    @SneakyThrows
    public void unbindTaskFromProject(@NotNull final String userId, @Nullable final String taskId) {
        if (isEmpty(taskId)) throw new EmptyIdException();
        try (Connection connection = connectionService.getConnection()) {
            @NotNull final TaskRepository taskRepository = new TaskRepository(connection);
            taskRepository.unbindTaskFromProject(userId, taskId);
        }
    }

}

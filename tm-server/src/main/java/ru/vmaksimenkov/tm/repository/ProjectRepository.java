package ru.vmaksimenkov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.IProjectRepository;
import ru.vmaksimenkov.tm.constant.FieldConst;
import ru.vmaksimenkov.tm.model.Project;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Nullable
    @SneakyThrows
    private Project fetch(@Nullable final ResultSet row) {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setId(row.getString(FieldConst.ID));
        project.setName(row.getString(FieldConst.NAME));
        project.setDescription(row.getString(FieldConst.DESCRIPTION));
        project.setDateStart(row.getDate(FieldConst.DATE_START));
        project.setDateFinish(row.getDate(FieldConst.DATE_FINISH));
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll(@NotNull String userId) {
        @NotNull final String query = "SELECT * FROM `app_project` WHERE `"+FieldConst.USER_ID+"` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Override
    @SneakyThrows
    public boolean existsById(@NotNull String userId, @NotNull String id) {
        if (isEmpty(id)) return false;
        @NotNull final String query = "SELECT COUNT(*) FROM `app_project` WHERE `"+FieldConst.USER_ID+"` = ? AND `"+FieldConst.ID+"` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        if (!resultSet.next()) return false;
        return resultSet.getInt(1) >= 1;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        @NotNull final Statement statement = getConnection().createStatement();
        @NotNull final String query = "SELECT * FROM `app_project`";
        @NotNull final ResultSet resultSet = statement.executeQuery(query);
        @NotNull final List<Project> result = new ArrayList<>();
        while (resultSet.next()) result.add(fetch(resultSet));
        statement.close();
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findById(@NotNull String id) {
        if (isEmpty(id)) return null;
        @NotNull final String query = "SELECT * FROM `app_project` WHERE `"+FieldConst.ID+"` = ? LIMIT 1";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final boolean hasNext = resultSet.next();
        if (!hasNext) return null;
        return fetch(resultSet);
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull String userId, @NotNull String id) {
        if (isEmpty(id)) return;
        @NotNull final String query = "DELETE FROM `app_project` WHERE `"+FieldConst.USER_ID+"` = ? AND `"+FieldConst.ID+"` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.setString(2, id);

    }

    public void createProject(@Nullable String name) {
        @NotNull Project project = new Project();
        project.setName(name);
        add(project);
    }

    @SneakyThrows
    public void update(@Nullable final Project project) {
        if (project == null) return;
        @NotNull final String query =
                "UPDATE `app_project` " +
                "SET `name` = ?, `description` = ?, `dateStart` = ?, `dateFinish` = ?, `"+FieldConst.USER_ID+"` = ? " +
                "WHERE `id` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, project.getName());
        statement.setString(2, project.getDescription());
        statement.setDate(3, prepare(project.getDateStart()));
        statement.setDate(4, prepare(project.getDateFinish()));
        statement.setString(5, project.getUserId());
        statement.setString(6, project.getId());
        statement.execute();
    }

    @Nullable
    @SneakyThrows
    public Project insert(@Nullable final Project project) {
        if (project == null) return null;
        if (project.getId() == null || project.getId().isEmpty()) return null;
        @NotNull final String query =
                "INSERT INTO `app_project` " +
                "(`"+FieldConst.NAME+"`, `description`, `dateStart`, `dateFinish`, `"+FieldConst.USER_ID+"`, `id`) " +
                "VALUES (?, ?, ?, ?, ?, ?)";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, project.getName());
        statement.setString(2, project.getDescription());
        statement.setDate(3, prepare(project.getDateStart()));
        statement.setDate(4, prepare(project.getDateFinish()));
        statement.setString(5, project.getUserId());
        statement.setString(6, project.getId());
        statement.execute();
        return project;
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final String query = "DELETE FROM `app_project`";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.execute();
    }

    @Override
    public boolean existsByName(@NotNull final String userId, @NotNull final String name) {
        return list.stream()
                .anyMatch(e -> userId.equals(e.getUserId()) && name.equals(e.getName()));
    }

    @Nullable
    @Override
    public Project findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .skip(index - 1)
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public Project findOneByName(@NotNull final String userId, @NotNull final String name) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public String getIdByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Project project = list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .skip(index - 1)
                .findFirst()
                .orElse(null);
        return project != null ? project.getId() : null;
    }

    @Nullable
    @Override
    public String getIdByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Project project = list.stream()
                .filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName()))
                .findFirst()
                .orElse(null);
        return project != null ? project.getId() : null;
    }

    @Override
    public void removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        remove(list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .skip(index - 1)
                .findFirst()
                .orElse(null)
        );
    }

    @Override
    public void removeOneByName(@NotNull final String userId, @NotNull final String name) {
        list.remove(list.stream()
                .filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName()))
                .findFirst()
                .orElse(null)
        );
    }

}

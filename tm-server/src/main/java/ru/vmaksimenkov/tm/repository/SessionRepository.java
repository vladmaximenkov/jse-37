package ru.vmaksimenkov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.ISessionRepository;
import ru.vmaksimenkov.tm.model.Session;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull Connection connection) {
        super(connection);
    }

    @Override
    public void update(@Nullable Session entity) {

    }

    @Override
    public boolean contains(@NotNull final String id) {
        return list.stream().anyMatch(e -> id.equals(e.getId()));
    }

    @Override
    public List<Session> findByUserId(@NotNull String userId) {
        return list.stream().filter(e -> userId.equals(e.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public void removeByUserId(@NotNull String userId) {
        @NotNull final List<Session> operatedList = new ArrayList<>();
        list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .forEach(operatedList::add);
        list.removeAll(operatedList);
    }

}

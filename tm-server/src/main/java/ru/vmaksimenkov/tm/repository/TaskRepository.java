package ru.vmaksimenkov.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.api.repository.ITaskRepository;
import ru.vmaksimenkov.tm.constant.FieldConst;
import ru.vmaksimenkov.tm.model.Project;
import ru.vmaksimenkov.tm.model.Task;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static ru.vmaksimenkov.tm.util.ValidationUtil.isEmpty;

public class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @SneakyThrows
    public void update(@Nullable final Task task) {
        if (task == null) return;
        @NotNull final String query =
                "UPDATE `app_task` " +
                        "SET `name` = ?, `description` = ?, `dateStart` = ?, `dateFinish` = ?, `"+ FieldConst.USER_ID+"` = ? " +
                        "WHERE `id` = ?";
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, task.getName());
        statement.setString(2, task.getDescription());
        statement.setDate(3, prepare(task.getDateStart()));
        statement.setDate(4, prepare(task.getDateFinish()));
        statement.setString(5, task.getUserId());
        statement.setString(6, task.getId());
        statement.execute();
    }

    @Override
    public void bindTaskPyProjectId(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()) && taskId.equals(e.getId()))
                .findFirst()
                .ifPresent(e -> e.setProjectId(projectId));
    }

    @Override
    public boolean existsByName(@NotNull final String userId, @NotNull final String name) {
        return list.stream()
                .anyMatch(e -> userId.equals(e.getUserId()) && name.equals(e.getName()));
    }

    @Override
    public boolean existsByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return list.stream()
                .anyMatch(e -> userId.equals(e.getUserId()) && projectId.equals(e.getProjectId()));
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()) && projectId.equals(e.getProjectId()))
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public Task findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .skip(index - 1)
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public Task findOneByName(@NotNull final String userId, @NotNull final String name) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    @Override
    public String getIdByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final Task task = list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .skip(index - 1)
                .findFirst()
                .orElse(null);
        return task != null ? task.getId() : null;
    }

    @Override
    public void removeAllBinded(@NotNull final String userId) {
        @NotNull final List<Task> operatedList = new ArrayList<>();
        list.stream()
                .filter(e -> userId.equals(e.getUserId()) && !isEmpty(e.getProjectId()))
                .forEach(operatedList::add);
        list.removeAll(operatedList);
    }

    @Override
    public void removeAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()) && projectId.equals(e.getProjectId()))
                .forEach(this::remove);
    }

    @Override
    public void removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        remove(list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .skip(index - 1)
                .findFirst()
                .orElse(null)
        );
    }

    @Override
    public void removeOneByName(@NotNull final String userId, @NotNull final String name) {
        remove(list.stream()
                .filter(e -> userId.equals(e.getUserId()) && name.equals(e.getName()))
                .findFirst()
                .orElse(null)
        );
    }

    @Override
    public void unbindTaskFromProject(@NotNull final String userId, @NotNull final String taskId) {
        @Nullable final Task task = findById(userId, taskId);
        if (task == null) return;
        task.setProjectId(null);
    }

}
